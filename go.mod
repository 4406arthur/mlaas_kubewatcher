module gitlab.com/4406arthur/mlaas_kubewatcher

go 1.14

require (
	github.com/segmentio/kafka-go v0.3.7
	k8s.io/api v0.18.5
	k8s.io/apimachinery v0.18.5
	k8s.io/client-go v0.18.5
	k8s.io/code-generator v0.18.5
	k8s.io/klog/v2 v2.3.0
)
